'use strict';

module.exports = (grunt) => {

	grunt.initConfig({

		sass : {
			options: {
                outputStyle: 'expanded',
                // implementation: sass,
				sourceMap: false
			},
			dist : {
				files: {
					'dist/assets/css/main.css' : 'src/styles/template.scss'
				}
			}
		},

        prettify: {
            options: {
                "indent": 2,
                "indent_char": " ",
                "indent_scripts": "normal",
                "wrap_line_length": 0,
                "brace_style": "collapse",
                "preserve_newlines": true,
                "max_preserve_newlines": 1,
                "unformatted": [
                    "a",
                    "code",
                    "pre"
                ]
            },
            files: {
                src: 'dist/index.html',
                dest: 'dist/index.html'
            }
        },

        watch : {
            options: {
                livereload: true
            },
			css : {
				files: ['src/styles/**/*.scss'],
				tasks: ['sass'],
				options: {
					spawn: false
				}
			}
		}

	});

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-prettify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('server', ['watch']);

}